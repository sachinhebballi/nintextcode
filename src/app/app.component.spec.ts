import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { MaterialModule } from './shared/material.module';
import { FlightComponent } from './flights/flight/flight.component';
import { FlightsComponent } from './flights/flights/flights.component';
import { FlightSearchComponent } from './flights/flight-search/flight-search.component';
import { FlightFilterComponent } from './flights/flight-filter/flight-filter.component';
import { FlightsService } from './flights/services/flights.service';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MaterialModule,
        HttpClientModule,
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        FlightsComponent,
        FlightSearchComponent,
        FlightFilterComponent,
        FlightComponent
      ],
      providers: [FlightsService]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'Flights'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Flights');
  });
});
