import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { FlightComponent } from './flight.component';
import { MaterialModule } from '../../shared/material.module';

describe('FlightComponent', () => {
  let component: FlightComponent;
  let fixture: ComponentFixture<FlightComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FlightComponent ],
      imports: [MaterialModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const flight = {
      AirlineName: 'Emirates',
      AirlineLogoAddress: '',
      InboundFlightsDuration: '12:00',
      OutboundFlightsDuration: '14:00',
      Stops: 1,
      TotalAmount: 15000
    };

    component.flight = flight;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should bind the flight information to the appropriate fields', () => {
    const de = fixture.debugElement;

    const airlineElement = de.query(By.css('.name'));
    const inboundElement = de.query(By.css('.inbound'));
    const stopsElement = de.query(By.css('.stops'));
    const totalAmountElement = de.query(By.css('.total-amount'));

    expect(airlineElement.nativeElement.innerText).toContain('Emirates');
    expect(inboundElement.nativeElement.innerText).toContain('12:00');
    expect(stopsElement.nativeElement.innerText).toContain('1');
    expect(totalAmountElement.nativeElement.innerText).toContain('15000');
  });
});
