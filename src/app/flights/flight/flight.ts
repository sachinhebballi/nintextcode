export interface Flight {
    AirlineLogoAddress: string;
    AirlineName: string;
    InboundFlightsDuration: string;
    OutboundFlightsDuration: string;
    Stops: number;
    TotalAmount: number;
}
