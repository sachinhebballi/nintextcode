import { Component, Input } from '@angular/core';
import { Flight } from './flight';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.scss']
})
export class FlightComponent {
  @Input() flight: Flight;
  @Input() includeReturn: boolean;
}
