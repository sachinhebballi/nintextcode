import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { Airline } from './airline';

@Component({
  selector: 'app-flight-filter',
  templateUrl: './flight-filter.component.html',
  styleUrls: ['./flight-filter.component.scss']
})
export class FlightFilterComponent implements OnInit {
  @Input() airlines = [];
  @Input() maxStops: number;
  @Input() maxTotalAmount: number;

  @Output() filter = new EventEmitter();

  airline: Airline;

  flightsFilterForm: FormGroup;
  filterCriteria = {
    airlines: [],
    stops: 0,
    fareAmount: 0
  };

  minStops = 0;
  stepsStops = 1;
  minFare = 0;
  stepsFare = 1;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.flightsFilterForm = this.formBuilder.group({
      airline: [''],
      stops: [0],
      fareAmount: [0]
    });
  }

  filterFlights(airline: Airline) {
    if (!!airline) {
      this.addAirline(airline);
    }

    this.filter.emit(this.filterCriteria);
  }

  private addAirline(airline) {
    const index = this.filterCriteria.airlines.indexOf(airline);

    if (airline.Checked && index === -1) {
        this.filterCriteria.airlines.push(airline);
    } else if (index !== -1) {
        this.filterCriteria.airlines.splice(index, 1);
    }
  }
}
