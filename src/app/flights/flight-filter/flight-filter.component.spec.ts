import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FlightFilterComponent } from './flight-filter.component';
import { MaterialModule } from '../../shared/material.module';

describe('FlightFilterComponent', () => {
  let component: FlightFilterComponent;
  let fixture: ComponentFixture<FlightFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlightFilterComponent],
      imports: [BrowserAnimationsModule, MaterialModule, HttpClientModule, ReactiveFormsModule, FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightFilterComponent);
    component = fixture.componentInstance;

    component.ngOnInit();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set up flight filter form', () => {
    expect(component.flightsFilterForm.contains('airline')).toBeTruthy();
    expect(component.flightsFilterForm.contains('stops')).toBeTruthy();
    expect(component.flightsFilterForm.contains('fareAmount')).toBeTruthy();
  });

  it('should raise filter event when flights are filtered by airline', () => {
    let filterCriteria = null;

    component.filter.subscribe(filter => {
      filterCriteria = filter;

      expect(filterCriteria).not.toBeNull();
    });
  });
});
