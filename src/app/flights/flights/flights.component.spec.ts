import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { of } from 'rxjs/internal/observable/of';

import { MaterialModule } from '../../shared/material.module';

import { FlightsComponent } from './flights.component';
import { FlightSearchComponent } from '../flight-search/flight-search.component';
import { FlightFilterComponent } from '../flight-filter/flight-filter.component';
import { FlightComponent } from '../flight/flight.component';
import { FlightsService } from '../services/flights.service';

describe('FlightsComponent', () => {
  let component: FlightsComponent;
  let fixture: ComponentFixture<FlightsComponent>;
  let service: FlightsService;

  const flight = {
    AirlineName: 'Emirates',
    AirlineLogoAddress: '',
    InboundFlightsDuration: '12:00',
    OutboundFlightsDuration: '14:00',
    Stops: 1,
    TotalAmount: 15000
  };

  const searchCriteria = {
    source: 'SFO',
    destination: 'MEL',
    departureDate: '12/12/2020',
    returnDate: ''
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlightsComponent, FlightComponent, FlightSearchComponent, FlightFilterComponent],
      imports: [BrowserAnimationsModule, MaterialModule, HttpClientModule, ReactiveFormsModule, FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightsComponent);
    component = fixture.componentInstance;
    service = TestBed.get(FlightsService);
    spyOn(service, 'searchFlights').and.returnValue(of([flight]));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should search flights based on the input provided', () => {
    component.searchFlights(searchCriteria);

    expect(component.flights).toBeDefined();
    expect(component.flights.length).toBe(1);
    expect(component.flights[0]).toBe(flight);
  });

  it('should get airline names based on the flights returned', () => {
    component.searchFlights(searchCriteria);

    expect(component.airlines).toBeDefined();
    expect(component.airlines.length).toBe(1);
    expect(component.airlines[0].AirlineName).toBe(flight.AirlineName);
  });

  it('should set include return flag to false when the return date is empty', () => {
    component.searchFlights(searchCriteria);

    expect(component.includeReturn).toBeFalsy();
  });

  it('should get maximun stops based on the flights returned', () => {
    component.searchFlights(searchCriteria);

    expect(component.maxStops).toBe(1);
  });

  it('should get maximun fare amount based on the flights returned', () => {
    component.searchFlights(searchCriteria);

    expect(component.maxTotalAmount).toBe(15000);
  });

});
