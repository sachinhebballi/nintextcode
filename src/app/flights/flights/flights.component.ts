import { Component, OnDestroy } from '@angular/core';
import { FlightsService } from '../services/flights.service';
import { Flight } from '../flight/flight';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnDestroy {
  flightSubscription: Subscription;
  showFilter = false;
  flights: Array<Flight>;
  airlines = [];
  maxTotalAmount: number;
  maxStops: number;
  includeReturn: boolean;

  private allFlights: Array<Flight>;

  constructor(private flightsService: FlightsService) { }

  searchFlights(flightSearchDetails) {
    this.includeReturn = !!flightSearchDetails.returnDate;
    this.flightSubscription = this.flightsService.searchFlights(flightSearchDetails).subscribe((flights) => {
      this.showFilter = true;

      this.flights = flights;
      this.allFlights = flights; // saving this in another variable, so that we need not fetch again during filtering.

      // Gets unique airlines from the list of all airlines
      // Used for filters
      this.airlines = this.flights
        .map((flight: Flight) => flight.AirlineName)
        .filter((val, index, self) => self.indexOf(val) === index)
        .map((airline: string) => ({ AirlineName: airline, Checked: false }));

      // Gets the maximum total amount
      // Used for filters
      this.maxTotalAmount = Math.max(...this.flights.map((flight: Flight) => flight.TotalAmount), 0);

      // Gets the maximum number of stops
      // Used for filters
      this.maxStops = Math.max(...this.flights.map((flight: Flight) => flight.Stops), 0);
    });
  }

  filterFlights(flightSearchCriteria) {
    if (!!flightSearchCriteria) {
      this.flights = this.allFlights
        .filter((flight: Flight) => (flightSearchCriteria.airlines.length === 0 ||
                                    flightSearchCriteria.airlines.length > 0 &&
                                    flightSearchCriteria.airlines.filter(x => x.AirlineName === flight.AirlineName).length > 0) &&
                                    flight.Stops >= flightSearchCriteria.stops &&
                                    flight.TotalAmount >= flightSearchCriteria.fareAmount);
    } else {
      this.flights = this.allFlights;
    }
  }

  ngOnDestroy() {
    this.flightSubscription.unsubscribe();
  }
}
