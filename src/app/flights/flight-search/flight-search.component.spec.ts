import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FlightSearchComponent } from './flight-search.component';
import { MaterialModule } from '../../shared/material.module';

describe('FlightSearchComponent', () => {
  let component: FlightSearchComponent;
  let fixture: ComponentFixture<FlightSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FlightSearchComponent],
      imports: [BrowserAnimationsModule, MaterialModule, HttpClientModule, ReactiveFormsModule, FormsModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightSearchComponent);
    component = fixture.componentInstance;

    component.ngOnInit();

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set up flight search form', () => {
    expect(component.flightSearchForm.contains('source')).toBeTruthy();
    expect(component.flightSearchForm.contains('destination')).toBeTruthy();
    expect(component.flightSearchForm.contains('departureDate')).toBeTruthy();
    expect(component.flightSearchForm.contains('returnDate')).toBeTruthy();
  });

  it('should throw validation error when source is empty', () => {
    const sourceControl = component.sourceControl;
    sourceControl.setValue('');

    expect(sourceControl.valid).toBeFalsy();
  });

  it('should throw validation error when destination is empty', () => {
    const destinationControl = component.destinationControl;
    destinationControl.setValue('');

    expect(destinationControl.valid).toBeFalsy();
  });

  it('should throw validation error when departureDate is empty', () => {
    const departureDateControl = component.departureDateControl;
    departureDateControl.setValue('');

    expect(departureDateControl.valid).toBeFalsy();
  });

  it('should raise search event when searched', () => {
    let searchCriteria = null;

    component.searchResult.subscribe(search => {
      searchCriteria = search;

      expect(searchCriteria).not.toBeNull();
    });
  });
});
