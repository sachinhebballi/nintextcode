import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-flight-search',
  templateUrl: './flight-search.component.html',
  styleUrls: ['./flight-search.component.scss']
})
export class FlightSearchComponent implements OnInit {
  @Output() searchResult = new EventEmitter();

  flightSearchForm: FormGroup;
  minDate = new Date();

  constructor(private formBuilder: FormBuilder) { }

  get sourceControl() {
    return this.flightSearchForm.get('source');
  }

  get destinationControl() {
    return this.flightSearchForm.get('destination');
  }

  get departureDateControl() {
    return this.flightSearchForm.get('departureDate');
  }

  get returnDateControl() {
    return this.flightSearchForm.get('returnDate');
  }

  ngOnInit() {
    this.flightSearchForm = this.formBuilder.group({
      source: ['', Validators.compose([Validators.maxLength(3), Validators.required])],
      destination: ['', Validators.compose([Validators.maxLength(3), Validators.required])],
      departureDate: ['', Validators.required],
      returnDate: ['']
    });

    this.flightSearchForm.get('departureDate').valueChanges.subscribe((value: Date) => {
      this.flightSearchForm.get('returnDate').setValue('');
    });
  }

  search() {
    const flightSearchDetails = this.flightSearchForm.value;
    this.searchResult.emit(flightSearchDetails);
  }
}
