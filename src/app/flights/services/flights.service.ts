import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Flight } from '../flight/flight';

@Injectable({
  providedIn: 'root'
})
export class FlightsService {
  constructor(private http: HttpClient) {}

  searchFlights(searchDetails): any {
    const uri = `${environment.flightsUri}/flight`;
    return this.http.get<Flight>(uri, {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });
  }
}
