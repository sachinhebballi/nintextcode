import { TestBed, getTestBed } from '@angular/core/testing';

import { FlightsService } from './flights.service';
import { HttpClientModule } from '@angular/common/http';

import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from 'src/environments/environment';

describe('FlightsService', () => {
  let service: FlightsService;
  let httpMock: HttpTestingController;
  let flight = {
    AirlineName: 'Emirates',
    AirlineLogoAddress: '',
    InboundFlightsDuration: '12:00',
    OutboundFlightsDuration: '14:00',
    Stops: 1,
    TotalAmount: 15000
  };

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientModule, HttpClientTestingModule],
    providers: [FlightsService]
  }));

  beforeEach(() => {
    service = TestBed.get(FlightsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should search flights', () => {
    environment.flightsUri = 'http://nmflightapi.azurewebsites.net/api';
    const searchDetails = {};

    service.searchFlights(searchDetails).subscribe((flights) => {
      expect(flights).toBeDefined();
      expect(flights.length).toBe(1);
      expect(flights[0]).toBe(flight);
    });

    const req = httpMock.expectOne(`${environment.flightsUri}/flight`);
    req.flush([flight]);
  });
});
