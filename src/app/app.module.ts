import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlightComponent } from './flights/flight/flight.component';
import { MaterialModule } from './shared/material.module';
import { FlightsService } from './flights/services/flights.service';
import { FlightsComponent } from './flights/flights/flights.component';
import { HttpClientModule } from '@angular/common/http';
import { FlightSearchComponent } from './flights/flight-search/flight-search.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlightFilterComponent } from './flights/flight-filter/flight-filter.component';

@NgModule({
  declarations: [
    AppComponent,
    FlightComponent,
    FlightsComponent,
    FlightSearchComponent,
    FlightFilterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [FlightsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
